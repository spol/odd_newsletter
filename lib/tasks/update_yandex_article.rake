desc "Fetches last article from Yandex and updates article in app"
task update_yandex_article: :environment do
  UpdateYandexArticleService.call
end
