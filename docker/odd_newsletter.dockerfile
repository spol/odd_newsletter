FROM ruby:2.4.2
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

# Nodejs
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - \
&& apt-get install -y nodejs

# Yarn
RUN apt-get update && apt-get install -y curl apt-transport-https wget && \
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
apt-get update && apt-get install -y yarn

# Cron
RUN apt-get install -y cron

RUN mkdir /odd_newsletter
WORKDIR /odd_newsletter

ADD Gemfile /odd_newsletter/Gemfile
ADD Gemfile.lock /odd_newsletter/Gemfile.lock
RUN bundle install --jobs=5

ADD . /odd_newsletter

RUN yarn install
RUN ./bin/webpack
