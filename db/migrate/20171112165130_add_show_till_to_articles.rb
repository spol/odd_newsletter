class AddShowTillToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :show_till, :datetime
  end
end
