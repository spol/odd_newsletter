class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.string :title, null: false
      t.string :annotation, null: false
      t.datetime :published_at, null: false
    end
  end
end
