Rails.application.routes.draw do
  root to: "articles#index"
  get "/current_article", to: "articles#current_article"

  namespace :admin do
    root to: "custom_articles#edit"
    put "/update", to: "custom_articles#update", as: :custom_article
    get "/current_custom_article", to: "custom_articles#current_custom_article"
  end
end
