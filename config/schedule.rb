ENV.each { |k, v| env(k, v) }

set :output, File.expand_path("../../log/cron_log.log", __FILE__)
set :environment, ENV['RAILS_ENV']

every 1.minutes do
  rake "update_yandex_article"
end
