class BroadcastArticleUpdatedJob < ActiveJob::Base
  # Set the Queue as Default
  queue_as :default

  def perform
    BroadcastService.call
  end
end
