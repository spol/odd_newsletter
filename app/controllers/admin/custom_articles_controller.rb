class Admin::CustomArticlesController < ApplicationController

  layout "admin"

  def edit; end

  def current_custom_article
    render json: current_custom_article_attributes
  end

  def update
    custom_article_form = CustomArticleForm.new(custom_article_form_params[:custom_article_form])

    if custom_article_form.valid?
      ArticleUpdateService.call(current_article, custom_article_form)
      render json: { success: true, message: "Article updated!" }
    else
      render json: { success: false, errors: custom_article_form.errors }, status: 422
    end
  end

  private

  def current_custom_article_attributes
    current_article.custom_active? ? current_article.attributes : {}
  end

  def current_article
    @current_article ||= Article.current
  end

  def custom_article_form_params
    params.permit(custom_article_form: {})
  end

end
