class ArticlesController < ApplicationController

  def index; end

  def current_article
    render json: article
  end

  private

  def article
    Article.current
  end

end
