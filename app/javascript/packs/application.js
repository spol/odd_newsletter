import Vue from 'vue/dist/vue.esm'
import ActionCable from 'actioncable'
import axios from 'axios'
import moment from 'moment'

const cable = ActionCable.createConsumer()

Vue.prototype.$cable = cable
Vue.prototype.$http = axios

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    el: '#currentArticle',
    data () {
      return {
        articleUpdatedChannel: null,
        article: {
          title: "N/A",
          annotation: "N/A",
          published_at: "N/A"
        }
      }
    },
    created(){
      this.fetchData()
      this.listenChannel()
    },
    methods: {

      fetchData() {
        var self = this
        this.$http.get("/current_article.json").then(function (response) {
          self.updateArticle(response.data)
        })
      },

      listenChannel() {
        var self = this
        this.articleUpdatedChannel = this.$cable.subscriptions.create({ channel: 'ArticleUpdatedChannel' }, {
          connected() { console.log("connected!") },
          received(data) {
            self.updateArticle(data)
          }
        })
      },

      updateArticle(data) {
        this.article.title = data.title
        this.article.annotation = data.annotation
        this.article.published_at = moment(data.published_at).format("YYYY-MM-DD HH:mm")
      }

    }

  })
})
