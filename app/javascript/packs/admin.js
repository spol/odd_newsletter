import Vue from 'vue/dist/vue.esm'
import axios from 'axios'
import moment from 'moment'

let token = document.getElementsByName('csrf-token')[0].getAttribute('content')
axios.defaults.headers.common['X-CSRF-Token'] = token
axios.defaults.headers.common['Accept'] = 'application/json'

Vue.prototype.$http = axios

document.addEventListener('DOMContentLoaded', () => {
    const app = new Vue({
        el: '#customArticle',
        data() {
            return {
                article: {
                    title: null,
                    annotation: null,
                    year: null,
                    month: null,
                    day: null,
                    hour: null,
                    minute: null
                },
                message: null,
                errors: {
                    title: null,
                    annotation: null,
                    show_till: null
                }
            }
        },
        created() {
            this.fetchData()
        },
        methods: {
            submit() {
                this.clearErrors()
                this.clearMessage()

                var self = this
                this.$http.put("/admin/update.json", this.formData())
                    .then(function (response) {
                        self.message = response.data.message
                    })
                    .catch(function (error) {
                        self.errors = error.response.data.errors
                    });
            },
            clearErrors(){
                this.errors = {
                    title: null,
                    annotation: null,
                    show_till: null
                }
            },
            clearMessage(){
                this.message = null
            },
            errorMessage(attribute) {
                return (this.errors[attribute] || []).join(", ")
            },
            fetchData() {
                var self = this
                this.$http.get("/admin/current_custom_article.json").then(function (response) {
                    self.updateArticle(response.data)
                })
            },
            updateArticle(data) {
                this.article.title = data.title
                this.article.annotation = data.annotation
                this.parseDate(data.show_till)
            },
            parseDate(dateString) {
                var res = moment(dateString)
                this.article.year = res.year()
                this.article.month = res.month() + 1
                this.article.day = res.date()
                this.article.hour = res.hour()
                this.article.minute = res.minute()
            },
            showTilldateTimeString() {
                var datetime = moment();
                datetime.set({
                    "year": this.article.year,
                    "month": this.article.month - 1,
                    "date": this.article.day,
                    "minute": this.article.minute,
                    "second": 0
                })

                return datetime.toString()
            },
            formData() {
                return {
                    custom_article_form: {
                        title: this.article.title,
                        annotation: this.article.annotation,
                        show_till: this.showTilldateTimeString()
                    }
                }
            }

        }

    })
})
