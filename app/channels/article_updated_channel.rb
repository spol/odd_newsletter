class ArticleUpdatedChannel < ApplicationCable::Channel
  def subscribed
    stream_from "article_updated"
  end
end
