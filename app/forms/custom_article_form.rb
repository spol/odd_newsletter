class CustomArticleForm
  extend ActiveModel::Naming

  include Virtus.model
  include ActiveModel::Validations

  attribute :title, String
  attribute :annotation, String
  attribute :show_till, DateTime
  attribute :published_at, DateTime, default: Proc.new { Time.zone.now }

  validates :title, length: { in: 10..20 }
  validates :annotation, length: { in: 30..100 }
  validate :valid_show_till_datetime

  def show_till=(value)
    new_value = Time.zone.parse(value.to_s)
    super(new_value)
  rescue ArgumentError
    nil
  end

  def to_key; end

  private

  def valid_show_till_datetime
    errors.add(:show_till, :invalid) if show_till.blank? || show_till <= Time.zone.now
  end

end
