require "rss"

class YandexArticleService
  RSS_URI = URI("https://news.yandex.ru/index.rss")
  YandexArticle = Struct.new(:title, :annotation, :published_at, :show_till)

  def self.call
    new.call
  end

  def initialize(http_client = Net::HTTP)
    @http_client = http_client
  end

  def call
    rss = http_client.get(RSS_URI)
    item = RSS::Parser.parse(rss).items.first

    unescaped_title = unescape_html(item.title)
    unescaped_description = unescape_html(item.description)

    YandexArticle.new(unescaped_title, unescaped_description, item.pubDate, nil)
  end

  private

  attr_reader :http_client

  # For some reason Yandex use double escaped html(&amp;quot; => ")
  def unescape_html(value)
    unescaped_first = CGI.unescapeHTML(value)
    CGI.unescapeHTML(unescaped_first)
  end
end
