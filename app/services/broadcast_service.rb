class BroadcastService
  STREAM_NAME = "article_updated"

  def self.call
    new.call
  end

  def initialize(action_cable_server = ActionCable.server)
    @action_cable_server = action_cable_server
  end

  def call
    article = Article.current

    action_cable_server.broadcast(
      STREAM_NAME,
      title: article.title,
      annotation: article.annotation,
      published_at: article.published_at
    )
  end

  private

  attr_reader :action_cable_server
end
