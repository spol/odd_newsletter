class ArticleUpdateService

  def self.call(article, article_params)
    new.call(article, article_params)
  end

  def initialize(broadcast_job = BroadcastArticleUpdatedJob)
    @broadcast_job = broadcast_job
  end

  def call(article, article_params)
    broadcast_job.perform_later if article.update_article(article_params)
  end

  private

  attr_reader :broadcast_job
end
