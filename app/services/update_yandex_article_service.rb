class UpdateYandexArticleService

  def self.call
    new.call
  end

  def initialize(yandex_article_service: YandexArticleService, article_update_service: ArticleUpdateService)
    @yandex_article_service = yandex_article_service
    @article_update_service = article_update_service
  end

  def call
    current_article = Article.current
    return if current_article.custom_active?

    yandex_article = yandex_article_service.call
    article_update_service.call(current_article, yandex_article)
  end

  private

  attr_reader :yandex_article_service
  attr_reader :article_update_service
end
