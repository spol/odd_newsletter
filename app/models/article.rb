class Article < ApplicationRecord

  class << self
    def current
      Article.first
    end
  end

  def update_article(article_attributes)
    self.title = article_attributes.title
    self.annotation = article_attributes.annotation
    self.published_at = article_attributes.published_at
    self.show_till = article_attributes.show_till

    changed? && self.save!
  end

  def custom_active?
    show_till.present? && show_till > Time.zone.now
  end
end
