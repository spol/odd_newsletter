describe Article do
  describe "#update_article" do
    let!(:article_params) do
      struct_klass = Struct.new(:title, :annotation, :published_at, :show_till)

      struct_klass.new(
        "title",
        "annotation",
        Time.zone.parse("2017-11-08 13:00:00 + 03:00"),
        Time.zone.parse("2017-11-09 13:00:00 + 03:00")
      )
    end

    subject { article.update_article(article_params)  }

    context "article was updated" do
      let(:article) { Article.new }

      it "returns true" do
        expect(subject).to eq(true)
      end

      it "updates article" do
        subject

        expect(article.title).to eq(article_params.title)
        expect(article.annotation).to eq(article_params.annotation)
        expect(article.published_at).to eq(article_params.published_at)
        expect(article.show_till).to eq(article_params.show_till)
      end
    end

    context "article was not updated" do
      let(:article) do
        Article.create(
          title: article_params.title,
          annotation: article_params.annotation,
          published_at: article_params.published_at,
          show_till: article_params.show_till
        )
      end

      it "returns false" do
        expect(subject).to eq(false)
      end

      it "does not updates article" do
        subject

        expect(article.title).to eq(article_params.title)
        expect(article.annotation).to eq(article_params.annotation)
        expect(article.published_at).to eq(article_params.published_at)
        expect(article.show_till).to eq(article_params.show_till)
      end
    end
  end

  describe "#custom_active?" do
    subject { article.custom_active? }

    context "when show_till is blank" do
      let(:article) { Article.new(show_till: nil) }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "when show_till is greater than current time" do
      let(:article) { Article.new(show_till: Time.zone.now + 1.hour) }

      it "returns true" do
        expect(subject).to eq(true)
      end
    end

    context "when show_till is lower than current time" do
      let(:article) { Article.new(show_till: Time.zone.now - 1.hour) }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end
  end
end
