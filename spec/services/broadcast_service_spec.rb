describe BroadcastService do
  describe "#call" do
    let(:broadcast_service) { BroadcastService.new(action_cable_server) }
    let(:action_cable_server) { double("action_cable_server") }
    let!(:article) { Article.create(title: "title", annotation: "annotation", published_at: Time.zone.parse("2017-11-14 14:00:00 +0300")) }

    subject { broadcast_service.call }

    it "calls action cable server" do
      expect(action_cable_server).to(
        receive(:broadcast).with(
          "article_updated",
          title: article.title,
          annotation: article.annotation,
          published_at: article.published_at
        )
      )

      subject
    end
  end
end
