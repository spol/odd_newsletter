describe YandexArticleService do
  describe ".call" do

    let(:http_client) do
      Class.new do
        def self.get(_uri)
          File.read(Rails.root.join("spec", "fixtures", "files", "yandex_feed.xml"))
        end
      end
    end

    let(:yandex_title) { "Трамп и Путин пожали друг другу руки на саммите АТЭС \"" }
    let(:yandex_annotation) { "Трамп подошел к Путину во время церемонии фотографирования в национальных костюмах, главы государств обменялись приветствиями." }
    let(:yandex_published_at) { "10 Nov 2017 17:19:12 +0300" }

    subject { YandexArticleService.new(http_client).call }

    it "returns last yandex article" do
      expect(subject.title).to eq(yandex_title)
      expect(subject.annotation).to eq(yandex_annotation)
      expect(subject.published_at).to eq(yandex_published_at)
      expect(subject.show_till).to eq(nil)
    end
  end
end
