describe UpdateYandexArticleService do
  describe "#call" do
    let(:update_yandex_article_service) do
      UpdateYandexArticleService.new(
        yandex_article_service: yandex_article_service,
        article_update_service: article_update_service
      )
    end

    let(:yandex_article) { double("yandex_article") }
    let(:yandex_article_service) { double("yandex_article_service") }
    let(:article_update_service) { double("article_update_service") }

    before do
      allow(yandex_article_service).to receive(:call).and_return(yandex_article)
      allow(article_update_service).to receive(:call).and_return(true)
    end

    subject { update_yandex_article_service.call }

    context "when custom article is not active" do
      let!(:article) { Article.create(title: "title", annotation: "annotation", published_at: Time.zone.now) }

      it "calls yandex_article_service" do
        expect(yandex_article_service).to receive(:call)
        subject
      end

      it "calls article_update_service" do
        expect(article_update_service).to receive(:call).with(article, yandex_article)
        subject
      end
    end

    context "when custom article is active" do
      let!(:article) { Article.create(title: "title", annotation: "annotation", published_at: Time.zone.now, show_till: 1.hour.since) }

      it "calls yandex_article_service" do
        expect(yandex_article_service).to_not receive(:call)
        subject
      end

      it "calls article_update_service" do
        expect(article_update_service).to_not receive(:call).with(article, yandex_article)
        subject
      end
    end

  end
end
