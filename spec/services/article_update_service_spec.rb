describe ArticleUpdateService do
  describe "#call" do
    let(:article) { double("article") }
    let(:article_params) { double("article_params") }
    let(:broadcast_job) { double("broadcast_job") }
    let(:article_update_service) { ArticleUpdateService.new(broadcast_job) }

    before { allow(article).to receive(:perform_later) }

    subject { article_update_service.call(article, article_params) }

    context "when article was updated" do
      before { allow(article).to receive(:update_article).with(article_params).and_return(true) }

      it "calls broadcast service" do
        expect(broadcast_job).to receive(:perform_later)
        subject
      end
    end

    context "when article was not updated" do
      before { allow(article).to receive(:update_article).with(article_params).and_return(false) }

      it "does not call broadcast service" do
        expect(broadcast_job).to_not receive(:perform_later)
        subject
      end
    end
  end

end
